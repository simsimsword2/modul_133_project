<?php
  session_start();
?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>LoginPage</title>
  <link rel="stylesheet" type="text/css" href="login.css">
</head>
<body>
  <div class=mainblock>
    <h1>Login</h1>

<?php

  // variables---------------------------------------------------------
  $host = 'localhost';
  $user = 'root';
  $password = '';
  $database = 'm133_hiredknight';


  // data source name (driver, host und DB)----------------------------
  $dsn = 'mysql:host=' . $host . ';dbname=' . $database;

  // DB connection
  $db = new PDO($dsn, $user, $password);
  $query = "SELECT *
              FROM tuser
             WHERE UserName = ?
           ";


  // filler data-------------------------------------------------------
  $formatEmail = '/^.{2,}@.{2,}\..{2,}$/';

  // init variables
  $email = $password = '';
  $fromForm = 0;

  // error flag and messages
  $EEmail = $EPassword = 0;
  $emailErr = $passwordErr = '';
  $focusEmail = $focusPassword = '';


  // logic-------------------------------------------------------------

  if (!isset($_SESSION['loginCounter'])) {

    $_SESSION['loginCounter'] = 0;
  }

  if (isset($_SESSION['SUserName'])) {
    $email = $_SESSION['SUserName'];
  }

  if (isset($_POST['send'])) {

    $fromForm = 1;

    // input sanitazion
    $email = htmlspecialchars($_POST['email']);
    $password = htmlspecialchars($_POST['password']);

    // find out if all fields are filled
    if ($password == '') {

      $EPassword = 1;
      $passwordErr = 'Password is required';
      $focusPassword = 'autofocus';
    }

    if ($email == '') {

      $EEmail = 1;
      $emailErr = 'Email is required';
      $focusEmail = 'autofocus';

    } else {

      if (! preg_match($formatEmail, $email)) {

        $EEmail = 1;
        $emailErr = 'Please use a valid email address: name@provider.com';
        $focusEmail = 'autofocus';
      }
    }

    //check if mail is in database and if password matches
    if (!$EEmail and !$EPassword) {

      $stmt = $db -> prepare($query);
      $stmt -> execute([$email]);
      $rowCount = $stmt -> rowCount();
      $result = $stmt -> fetch(PDO::FETCH_ASSOC);

      // email was not found on database
      if ($rowCount == 0) {
        
        $EEmail = 1;
        $emailErr = 'Invalid Email or password please try again';
        $focusEmail = 'autofocus';
        $_SESSION['loginCounter'] += 1;
      } else {

        // check if password matches
        if (!password_verify($password, $result['PassHash'])) {
          $EEmail = 1;
          $emailErr = 'Invalid Email or password please try again';
          $focusEmail = 'autofocus';
          $_SESSION['loginCounter'] += 1;
        } else {
          
          // login successful -> set session variables 
          $_SESSION['SUserId'] = $result['UserId'];
          $_SESSION['SUserName'] = $result['UserName'];
          $_SESSION['SFirstName'] = $result['FirstName'];
          $_SESSION['SSurname'] = $result['Surname'];
        }
      }      
    }
  }


  if (!$fromForm or $EEmail or $EPassword) {

    if ($_SESSION['loginCounter'] == 2){
      echo("You have one more login attempt left. If the next login fails, you will have to wait.");
    }

    if ($_SESSION['loginCounter'] >= 3) {
      sleep(2 + $_SESSION['loginCounter']);
    }

    showForm();
  } else {
    showConfirmation();
    $_SESSION['loginCounter'] = 0;
  }
  


  //-------------------------------------------------------------------
  // Functions
  // *********

  // Confirmation
  function showConfirmation() {
    
      // user is redirected to the order page
      header('Location: ../order/order.php');
      exit();    
  }

  // showForm
  function showForm() {
    ?>

        <p><span class="error">* required field</span></p>

        <form class=form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>" accept-charset="utf-8">

          <!-- Email field -->
          <label for="email">Email</label>
          <input type="text" name="email" value="<?php echo $GLOBALS['email'] ?>" size="50" <?php echo $GLOBALS['focusEmail'];?>>
          <span class="error">* <?php echo $GLOBALS['emailErr'];?></span>
          <br><br>

          <!-- Password field -->
          <label for="password">Password</label>
          <input type="password" name="password" size="50" minlength="8" <?php echo $GLOBALS['focusPassword'];?>>
          <span class="error">* <?php echo $GLOBALS['passwordErr'];?></span>
          <br><br>

          <!-- Send button -->
          <div class=buttoncenter>
            <input type="submit" name="send" value="send" class="button">
          </div>
        </form>

      </div>

      <footer>
          Designed and run by Florian and Simon <br>
          Technische Berufsschule Zürich Ausstellungsstrasse 70, 8090 Zürich <br>
          <a href="mailto:hiredknight.info@gmail.com">hiredknight.info@gmail.com</a>
          <div class=copyright>
              © All rights belong to Florian and Simon (30.10.2021)
          </div>
      </footer>
    <?php
  }

?>


</body>

</html>




