<?php

session_start();

// initialize variables
$armor = '';
$weapon = '';
$mount = '';
$fromForm = 0;
$sendEmail = 'hiredknight.info@gmail.com';

// initialize host variables
$email = $_SESSION['SUserName'];
$firstName = $_SESSION['SFirstName'];
$surName = $_SESSION['SSurname'];

// initialize error variables
$errorArmor = 0;
$errorWeapon = 0;
$errorMount = 0;

if (isset($_POST['send'])) {

    $fromForm = 1;

    if (!(isset($_POST['armor']))) {
        $errorArmor = 1;
    }else {
        $armor = $_POST['armor'];
    }

    if (!(isset($_POST['weapon']))) {
        $errorWeapon = 1;
    }else {
        $weapon = $_POST['weapon'];
    }

    if (!(isset($_POST['mount']))) {
        $errorMount = 1;
    }else {
        $mount = $_POST['mount'];
    }
}

// Print form with errors and existing information if a field hasen't been filled or the empty form is requested
if ($errorArmor or $errorMount or $errorWeapon or !$fromForm) {
    printHeadTitle();
    printForm();
// Send email and show order confirmation when all choices have been made
}else {
    sendEmail();
    printHeadTitle();
    printConfirmation();
}

// Functions

function printForm() {
    global $armor;
    global $weapon;
    global $mount;

    global $errorArmor;
    global $errorWeapon;
    global $errorMount;
    
    global $fromForm;

    $checkedNaked = '';
    $checkedLightArmor = '';
    $checkedHeavyArmor = '';
    $checkedSword = '';
    $checkedAxe = '';
    $checkedLance = '';
    $checkedLongbow = '';
    $checkedCrossbow = '';
    $checkedNone = '';
    $checkedDonkey = '';
    $checkedHorse = '';

    // If the request comes from the form and one or multiple options haven't been set
    if ($fromForm) {
        // Print error
        echo '<h2>Data is incomplete</h2>';
        echo '<p>Please also choose an option for those equipment parts:<br>';
        if ($errorArmor) {
            echo 'Armor<br>';
        }

        if ($errorWeapon) {
            echo 'Weapon<br>';
        }

        if ($errorMount) {
            echo 'Mount<br>';
        }

        echo '</p>';

        // Check which selections have already been made
        if ($armor == 'naked') {
            $checkedNaked = 'checked';
        }elseif ($armor == 'lightarmor') {
            $checkedLightArmor = 'checked';
        }elseif ($armor == 'heavyarmor') {
            $checkedHeavyArmor = 'checked';
        }
        
        if ($weapon == 'sword') {
            $checkedSword = 'checked';
        }elseif ($weapon == 'axe') {
            $checkedAxe = 'checked';
        }elseif ($weapon == 'lance') {
            $checkedLance = 'checked';
        }elseif ($weapon == 'longbow') {
            $checkedLongbow = 'checked';
        }elseif ($weapon == 'crossbow') {
            $checkedCrossbow = 'checked';
        }

        if ($mount == 'none') {
            $checkedNone = 'checked';
        }elseif ($mount == 'donkey') {
            $checkedDonkey = 'checked';
        }elseif ($mount == 'horse') {
            $checkedHorse = 'checked';
        }

    }

    // Print form
    echo <<<END_FORM
                <form class=form method="post" action="$_SERVER[PHP_SELF]" accept-charset="utf-8">
                    <fieldset>
                        <legend>Armor<span class="error">*</span></legend>
                        <div class="field">
                            <label><input type="radio" name="armor" value="naked" class="check-radio" $checkedNaked>Naked</label><br>
                            <label><input type="radio" name="armor" value="lightarmor" class="check-radio" $checkedLightArmor>Light Armor</label><br>
                            <label><input type="radio" name="armor" value="heavyarmor" class="check-radio" $checkedHeavyArmor>Heavy Armor</label><br>
                        </div>
                    </fieldset>

                    <fieldset>
                        <legend>Weapon<span class="error">*</span></legend>
                        <div class="field">
                            <label><input type="radio" name="weapon" value="sword" class="check-radio" $checkedSword>Sword</label><br>
                            <label><input type="radio" name="weapon" value="axe" class="check-radio" $checkedAxe>Axe</label><br>
                            <label><input type="radio" name="weapon" value="lance" class="check-radio" $checkedLance>Lance</label><br>
                            <label><input type="radio" name="weapon" value="longbow" class="check-radio" $checkedLongbow>Longbow</label><br>
                            <label><input type="radio" name="weapon" value="crossbow" class="check-radio" $checkedCrossbow>Crossbow</label><br>
                        </div>
                    </fieldset>

                    <fieldset>
                        <legend>Mount<span class="error">*</span></legend>
                        <div class="field">
                            <label><input type="radio" name="mount" value="none" class="check-radio" $checkedNone>None</label><br>
                            <label><input type="radio" name="mount" value="donkey" class="check-radio" $checkedDonkey>Donkey</label><br>
                            <label><input type="radio" name="mount" value="horse" class="check-radio" $checkedHorse>Horse</label><br>
                        </div>
                    </fieldset>
                    <div class=buttoncenter>
                        <input type="submit" name="send" value="send" class="button">
                        <p><span class="error">* Mandatory field</span></p>
                    </div>

                </form>
            </div>
            <footer>
                Designed and run by Florian and Simon <br>
                Technische Berufsschule Zürich Ausstellungsstrasse 70, 8090 Zürich <br>
                <a href="mailto:hiredknight.info@gmail.com">hiredknight.info@gmail.com</a>
                <div class=copyright>
                    © All rights belong to Florian and Simon (30.10.2021)
                </div>
            </footer>
        </body>
    </html>
    END_FORM;
}

// Send email
function sendEmail() {
    global $armor;
    global $weapon;
    global $mount;
    global $sendEmail;
    global $email;
    global $firstName;
    global $surName;
    // Mail content
    $mailbody = "Armor:\n\t$armor \nWeapon:\n\t$weapon \nMount:\n\t$mount\n\nThank you for your order!";

    mail($email,
            'Order confirmation',
            $mailbody,
            "From: $sendEmail\r\nContent-type: text/plain;
            charset=UTF-8\r\nReply-To:$email");
}

// print header and title
function printHeadTitle() {
header('Content-Type: text/html; charset=utf-8');
echo <<<'END_HEAD'
    <!DOCTYPE html>
    <html lang="de-CH">
    <head>
        <meta charset="utf-8">
        <title>Order your knight</title>
        <link rel="stylesheet" href="order.css" type="text/css">
    </head>
    <body>
        <div class=mainblock>
            <h1>Order your knight</h1>
END_HEAD;
}

// print confirmation
function printConfirmation() {
echo <<<END_CONFIRMATION
            <div class="confirmation">
                <h2>Order confirmation</h2>
                <p>Thank you for your Order</p>
                <p>A confirmation e-mail has been sent.</p>
                <div class=buttonconfirmation>
                    <button class=button onclick="window.location.href = '../order/order.php';">Place another order</button>
                    <button class=button onclick="window.location.href = '../logout/logout.php';">Logout</button>
                </div>
            </div>
        </div>
        <footer>
            Designed and run by Florian and Simon <br>
            Technische Berufsschule Zürich Ausstellungsstrasse 70, 8090 Zürich <br>
            <a href="mailto:hiredknight.info@gmail.com">hiredknight.info@gmail.com</a>
            <div class=copyright>
                © All rights belong to Florian and Simon (30.10.2021)
            </div>
        </footer>
    </body>
</html>
END_CONFIRMATION;
}
?>