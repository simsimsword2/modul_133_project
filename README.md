# HiredKnight - project for modul 133

This is a PHP based webservice. Inspired by our work for modules 326 & 120.
-> [HiredKnight](https://github.com/simsimsword2/HiredKnight)




## Goal

We wish to create a website for users to order customized soldiers.

- First the user must sign up.
- After signing up the user can login to their account.
- If the login was successful, the user can now order customized soldiers.
- The order placed by the user is confirmed with an email.


## Signing up 
When signing up the user must enter an email address, name, surname, password and confirm the password by entering it again.\
If any field is invalid, the user is informed and asked to retry. For example, an email address can only be used once.\
Once all entries are valid, a database entry is made. The password is hashed with salt bevor storage.\
![signUp](images/signUp.PNG) 


## Login
After signing up or if the user already has an account, the user may login.\
If the user just performed a sign up, the email address is saved in a session variable and it is filled into the corresponding field on the login screen.\
![login](images/login.PNG) 



## Ordering custom soldier
Ordering a custom soldier is very easy. The user can pick from three sets of radio buttons corresponding to armor weapon and mount accordingly.\
![order from](images/order.PNG) 


## Sending a confirmation mail

In order to send confirmation via mail we had to make some changes to the php.ini and sendmail.ini files in XAMPP.\
php.ini:
![php.ini](images/phpini.PNG)  

sendmail.ini:
![sendmail.ini](images/sendmailini.PNG) 

We created a gmail account to send Emails from.
As shown above we use the SMTP server from gmail.
We found out how to send mails with XAMPP from this stackoverflow question: https://stackoverflow.com/questions/15965376/how-to-configure-xampp-to-send-mail-from-localhost

The confirmation mail: 
![confirmation mail](images/confirmation.PNG) 



## Reflection Florian

For me it was really interesting to start using sessions and session variables. In the beginning it was a challenge to understand how exactly it worked but with time and trial and error we managed to set up sessions with php. Aswell centering stuff and align it so that it looked nice was a challenge when creating the css stylesheets for the pages.
In the end I am really happy with how we figured stuff out and came to solutions that worked. It's always a pleasure to work with Simon, because we can add our knowledge up and help each other.

## Reflection Simon
Working on this project with Florian was once again a great pleasure.\
I have not worked with sessions on websites before and because we coded this website from scratch, I got to learn a lot. I especially enjoyed coding the sign up and login in combination with the database.\
I found it very interesting to see how a password was encrypted and saved into the database. Session variables enable a whole new level of handling over multiple pages which was very inspiring the play around with.\
Whenever I had any questions or did not know something works, I got a lot of help from many different internet pages such as stackoferflow, w3schools and php.net. The sheer amount information can be absolutely overwhelming however when used correctly, the internet is a powerful asset to every software developer’s toolbox.


> by Flo and Sim
