-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 29, 2021 at 09:11 PM
-- Server version: 10.4.21-MariaDB
-- PHP Version: 8.0.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `m133_hiredknight`
--
CREATE DATABASE IF NOT EXISTS `m133_hiredknight` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `m133_hiredknight`;

--
-- Dumping data for table `tuser`
--

INSERT INTO `tuser` (`UserId`, `UserName`, `PassHash`, `FirstName`, `Surname`) VALUES(1, 'simon@gmail.com', '$2y$10$zQZSokWNNMzSByuJJpnOVO0BDrLnBrSqxyW.Rcmzs6OJBok6DRWnO', 'Simon', 'Ambuehl');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
