<?php
  session_start();
?>

<!DOCTXPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>SignupPage</title>
  <link rel="stylesheet" type="text/css" href="signup.css">
</head>
<body>
  
  <div class=mainblock>
    <h1>Signup</h1>

<?php

  $host = 'localhost';
  $user = 'root';
  $password = '';
  $database = 'm133_hiredknight';


  // data source name (driver, host und DB)----------------------------------------------
  $dsn = 'mysql:host=' . $host . ';dbname=' . $database;

  // DB connection
  $db = new PDO($dsn, $user, $password);
  $query = "INSERT INTO tuser(UserName, PassHash, FirstName, Surname)
                 VALUES      (       ?,        ?,         ?,       ?)
           ";


  // filler data-------------------------------------------------------------------------
  $formatEmail = '/^.{2,}@.{2,}\..{2,}$/';

  // init variables
  $name      = $surname      = $email      = $password      = $confPassword      = '';
  $fromForm  = 0;

  // error flag and messages
  $EName     = $ESurname     = $EEmail     = $EPassword     = $EConfPassword     = 0;
  $nameErr   = $surnameErr   = $emailErr   = $passwordErr   = $confPasswordErr   = '';
  $focusName = $focusSurname = $focusEmail = $focusPassword = $focusConfPassword = '';


  // logic-------------------------------------------------------------------------------
  if (isset($_POST['send'])) {

    $fromForm = 1;

    // input sanitazion
    $name         = htmlspecialchars($_POST['name']);
    $surname      = htmlspecialchars($_POST['surname']);
    $email        = htmlspecialchars($_POST['email']);
    $password     = htmlspecialchars($_POST['password']);
    $confPassword = htmlspecialchars($_POST['confPassword']);

    // find out if all fields are filled
    if ($confPassword == '') {

      $EConfPassword = 1;
      $confPasswordErr = 'Password confirmation is required';
      $focusConfPassword = 'autofocus';
    }

    if ($password == '') {

      $EPassword = 1;
      $passwordErr = 'Password is required';
      $focusPassword = 'autofocus';
    }

    if (!$EPassword and !$EConfPassword) {

      // if the password was confirmed, hash it
      if ($password == $confPassword) {

        $hashPassword = password_hash($password, PASSWORD_DEFAULT);
      
      // else ask the user to confirm again  
      } else {

        $EConfPassword = 1;
        $confPasswordErr = 'Password confirmation must match password';
        $focusConfPassword = 'autofocus';
      }
    }

    if ($email == '') {

      $EEmail = 1;
      $emailErr = 'Email is required';
      $focusEmail = 'autofocus';

    } else {

      if (! preg_match($formatEmail, $email)) {

        $EEmail = 1;
        $emailErr = 'Please use a valid email address: name@provider.com';
        $focusEmail = 'autofocus';
      }
    }

    if ($surname == '') {

      $ESurname = 1;
      $surnameErr = 'Please enter your surname';
      $focusSurname = 'autofocus';
    } 
    
    if ($name == '') {

      $EName = 1;
      $nameErr = 'Please enter your name';
      $focusName = 'autofocus';
    } 

    // insert into DB
    // if the insert fails, the username(email) is a duplicate
    if (!$EName and !$ESurname and !$EEmail and !$EPassword and !$EConfPassword) {

      $stmt = $db -> prepare($query);

      try {

        $stmt -> execute([$email, $hashPassword, $name, $surname]);
          
      } catch(PDOException $Exception) {

        $EEmail = 1;
        $emailErr = 'This email is already in use';
        $focusEmail = 'autofocus';
      }           
    }
  }

  // if there is an error -> ask user to try again
  //
  // else -> confirmation window
  if (!$fromForm or $EName or $ESurname or $EEmail or $EPassword or $EConfPassword) {
    showForm();
  } else {
    $_SESSION['SUserName'] = $email;
    showConfirmation();
  }


  //-------------------------------------------------------------------------------------
  // Functions
  // *********

  // Confirmation
  function showConfirmation() {

    // user is redirected to the login page
    header('Location: ../login/login.php');
    exit();    
  }

  // showForm
  function showForm() {
    ?>

       <p><span class="error">* required field</span></p>

        <form class=form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>" accept-charset="utf-8">

          <!-- Name field -->
          <label for="name">Name</label>
          <input type="text" name="name" value="<?php echo $GLOBALS['name'] ?>" size="50" <?php echo $GLOBALS['focusName'];?>>
          <span class="error">* <?php echo $GLOBALS['nameErr'];?></span>
          <br><br>

          <!-- Surname field -->
          <label for="surname">Surname</label>
          <input type="text" name="surname" value="<?php echo $GLOBALS['surname'] ?>" size="50" <?php echo $GLOBALS['focusSurname'];?>>
          <span class="error">* <?php echo $GLOBALS['surnameErr'];?></span>
         <br><br>        

          <!-- Email field -->
          <label for="email">Email</label>
          <input type="text" name="email" value="<?php echo $GLOBALS['email'] ?>" size="50" <?php echo $GLOBALS['focusEmail'];?>>
          <span class="error">* <?php echo $GLOBALS['emailErr'];?></span>
          <br><br>

          <!-- Password field -->
          <label for="password">Password</label>
          <input type="password" name="password" value="<?php echo $GLOBALS['password'] ?>" size="50" minlength="8" <?php echo $GLOBALS['focusPassword'];?>>
          <span class="error">* <?php echo $GLOBALS['passwordErr'];?></span>
         <br><br>

          <!-- Password confirmation field -->
          <label for="confPassword">Confirm password</label>
          <input type="password" name="confPassword" size="50" minlength="8" <?php echo $GLOBALS['focusConfPassword'];?>>
          <span class="error">* <?php echo $GLOBALS['confPasswordErr'];?></span>
          <br><br>        

          <!-- Send button -->
          <div class=buttoncenter>
            <input type="submit" name="send" value="send" class="button">
          </div>
        </form>
      </div>
    <?php
  }

?>

<footer>
    Designed and run by Florian and Simon <br>
    Technische Berufsschule Zürich Ausstellungsstrasse 70, 8090 Zürich <br>
    <a href="mailto:hiredknight.info@gmail.com">hiredknight.info@gmail.com</a>
    <div class=copyright>
      © All rights belong to Florian and Simon (30.10.2021)
    </div>
  </footer>

</body>

</html>




